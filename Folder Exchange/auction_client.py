
import socket
import sys
import threading
import inquirer
import time
from datetime import datetime
import struct
import json

vehicles = []

def connectServer(ServerAddress, ServerPort):
    
    #Socket aufbauen
    ClientSocket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    # Message senden
    message = "Hallo Server ich moechte teilnehmen."
    ClientSocket.sendto(str.encode(message), (ServerAddress, ServerPort))
    #Auf Rückmeldung warten
    message, server = ClientSocket.recvfrom(1024)
    print(message.decode())

    ClientSocket_Bid = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    message, server = ClientSocket.recvfrom(1024)
    vehicles = message.decode().split(";")
    for i in vehicles:
        print(i)

    while True:

        userInput = input("Bitte Fahrzeugauswählen: ")

        while True:
            bid = input("Bitte ein Gebot in Euro eingeben: ")
            if bid.isnumeric():
                ClientSocket_Bid.sendto(str.encode(userInput + ";" + str(bid)), (ServerAddress, 1235))
                break
        while True:
            message, server = ClientSocket.recvfrom(1024)
            vehicles = message.decode().split(";")
            for i in vehicles:
                print(i)
            break
    ClientSocket_Bid.close()
def MulticastSendAndReceive():
    # create socket
    message = "Hallo Multicast"
    multicast_group = ('224.1.1.1', 7000)
    multisend_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    multisend_sock.settimeout(5)
    multisend_sock.bind(("",7000))
    # set time to live message (network hps; 1 for local)
    ttl = struct.pack('b', 127)
    multisend_sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, ttl)
    mreq = struct.pack('4sl', socket.inet_aton("224.1.1.1"), socket.INADDR_ANY)
    multisend_sock.setsockopt(socket.IPPROTO_IP,socket.IP_ADD_MEMBERSHIP, mreq)
    # send my IP to other participants
    #multisend_sock.sendto(message.encode(), multicast_group)
    #print("Sent my IP to server group")

    while True:
        try:
            # receive reply data from the other participants
            reply, address = multisend_sock.recvfrom(1024)

            if reply:
                # decode received data
                reply_address = reply.decode()
                currentLeader = reply_address
                vehicles = reply.decode().split(";")
                PushBid()
        except socket.timeout:
            pass

def PushBid():
    ClientSocket_Bid = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    for i in vehicles:
        print(i)


    userInput = input("Bitte Fahrzeugauswählen: ")

    while True:
        bid = input("Bitte ein Gebot in Euro eingeben: ")
        if bid.isnumeric():
            ClientSocket_Bid.sendto(str.encode(userInput + ";" + str(bid)), (socket.gethostbyname(socket.gethostname()), 1235))
            break
    ClientSocket_Bid.close()


if __name__ == '__main__':
    vehicles = []
    # Bind the socket to the port
    server_address = socket.gethostbyname(socket.gethostname())
    server_port = 1234
    print("Starte Client und verbinde zu Server")
    thread1 = threading.Thread(target = connectServer, args=(server_address, server_port))
    thread1.start()

    thread2 = threading.Thread(target = MulticastSendAndReceive, args=())
    thread2.start()

    thread3 = threading.Thread(target = PushBid, args=())
    thread3.start()















class Client():
    def __init__(self):
        self.address = socket.gethostbyname(socket.gethostname()) 
        self.port = 7000
        self.message = "Hallo Server"

    def ConnectToBroadcast(self):
        broadcast_address = '255.255.255.255' 
        #Erstelle einen UDP-Socket
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) 
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1) 
        
        # Sende Daten an die Broadcast-Adresse und den Port    
        sock.sendto(self.address.encode(), (broadcast_address, self.port))
        sock.close()

    def Listen(self):
        # Broadcast-Adresse und Port
        broadcast_port = 5000
        
        # Erstelle einen UDP-Socket
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.bind((self.address, broadcast_port))
        
        # Setze den Socket, um Broadcast zu empfangen
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        
        while True:
            # Empfange Daten
            data, address = sock.recvfrom(1024)
            print(f"Empfangene Nachricht: {data.decode()} von {address}")

client = Client()            

thread1 = threading.Thread(target = client.ConnectToBroadcast)
#thread1.start()

thread2 = threading.Thread(target = client.Listen)
#thread2.start()