import socket
import threading
import time
from datetime import datetime
import struct
import json
# Server Klasse erstellt zum Verstehen: Server aufbauen
MULTICAST_GROUP_IP = '224.1.1.1'

clientList = []
auctionList = []

class Auction():
    def __init__(self, vehicle):
        self.auction_vehicle = vehicle
        self.biddingList = []
        self.address = ""
    # Verwalten von Bids der Client
    def acceptBid(self, bid, address):
        fbid = float(bid)

        if(self.biddingList == []):
            self.biddingList.append(fbid)
            self.address = address
            return
        print(fbid > self.biddingList[-1])
        if(self.biddingList[-1] < fbid):
            self.address = address
            self.biddingList.append(fbid)
            print(self.biddingList)
            return         

vehicle1 = Auction("Mercedes")
vehicle2 = Auction("BMW")

auctionList = [vehicle1, vehicle2]




class Server():
    def __init__(self):
        self.address = socket.gethostbyname(socket.gethostname()) # Lokale IP Adresse durch Funktionsaufruf gezogen
        self.message = "Hello World!"
        self.serverMessage = "Wants to connect"
        self.port = 10001
        #self.serverPort = 5000
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        #self.serverList = []
        #self.clientOffers = []
        #self.vehicles = []
        #self.bidList = []

    def acceptClient(self):
        ServerSocket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        ServerSocket.bind((socket.gethostname(), 1234))
        print("Server läuft!")

        while True:
            firstClientMessage, address = ServerSocket.recvfrom(1024)
            firstClientMessage = str(firstClientMessage)
            if firstClientMessage == "b'Hallo Server ich moechte teilnehmen.'":

                print(f"Verbindung zu {address} wurde hergestellt!")
                print (firstClientMessage)
                clientList.append(address)
                print(clientList)
                ServerSocket.sendto(str.encode("Willkommen zur Auktion!"), address)    
            auctionResponse = ""
            for i in auctionList:
                auctionResponse += i.auction_vehicle
                if len(i.biddingList) > 0:
                    auctionResponse += ", Aktuelles Gebot: " + str(max(i.biddingList)) + ";"
                else:
                    auctionResponse += ";"
            ServerSocket.sendto(str.encode(auctionResponse), address)

    def handleBid(self):
        ServerSocket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        ServerSocket.bind((socket.gethostname(), 1235))
        print("Auf Gebot warten...")

        while True:
            ClientSocket, address_bid = ServerSocket.recvfrom(1024)
            ClientSocket = str(ClientSocket)
            print ("Ausgabe HandleBid-Funktion:   " + ClientSocket)
            bid = ClientSocket.replace("'","").split(";")[-1]
            vehicle = ClientSocket.replace("'","").split(";")[0][1:]
            print (vehicle)
            auctionResponse = ""
            for i in auctionList:
                if i.auction_vehicle == vehicle:
                    i.acceptBid(bid, address_bid)
                    print ("Gebot wurde abgegeben")
                auctionResponse += i.auction_vehicle
                if len(i.biddingList) > 0:
                    auctionResponse += ", Aktuelles Gebot: " + str(max(i.biddingList)) + ";"
                else:
                    auctionResponse += ";"


            #for adrs in clientList:
                #ServerSocket.sendto(str.encode(auctionResponse), adrs)
            multicast_group = ('224.1.1.1', 7000)
            multisend_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            # set time to live message (network hps; 1 for local)
            ttl = struct.pack('b', 10)
            multisend_sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, ttl)
            multisend_sock.sendto(auctionResponse.encode(), multicast_group)
            print("Gebote werden zurückgeschickt\n\n")







          
          











    def MulticastListenAndReply(self):
        # if my IP is not in the server list add it
        if self.address not in self.serverList:
            self.serverList.append(self.address)

        # create socket bind to server address
        multilis_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        multilis_sock.bind(('', self.serverPort))

        # tell the os to add the socket to the multicast group
        group = socket.inet_aton(MULTICAST_GROUP_IP)
        mreg = struct.pack('4sL', group, socket.INADDR_ANY)
        multilis_sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreg)
        
        while True:
                # receive data from other participants
                data, address = multilis_sock.recvfrom(1024)

                if data:
                    # if you have data than decode
                    print(data.decode())

    def MulticastSendAndReceive(self):
        # create socket
        multicast_group = (MULTICAST_GROUP_IP, self.serverPort)
        multisend_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) 
        multisend_sock.settimeout(2)

        # set time to live message (network hps; 1 for local)
        ttl = struct.pack('b', 1)
        multisend_sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, ttl)
        

        # send my IP to other participants
        message = "Hello World"
        multisend_sock.sendto(message.encode(), multicast_group)
        

    def ListenForServer(self):# Methode für neue Server 
        self.socket.bind((self.address, self.port))
        print('Server listening for other servers... {}:{}'.format(self.address, self.port))

        while True:
            print('\nWaiting to receive new server...\n')

            data, address = self.socket.recvfrom(1024)
            print('Received message from server: ', self.address)
            print('Server Message: ', data.decode())

            if data:
                self.socket.sendto(str.encode(self.message), self.address)
                print('Replied to client: ', self.message)

server = Server()

if __name__== '__main__':
    server = Server()

    #thread1 = threading.Thread(target = server.MulticastListenAndReply)
    #thread1.start()

    #thread2 = threading.Thread(target = server.MulticastSendAndReceive)
    #thread2.start()

    #thread3 = threading.Thread(target = server.Listen)
    #thread3.start()

    thread4 = threading.Thread(target = server.acceptClient)
    thread4.start()

    thread5 = threading.Thread(target = server.handleBid)
    thread5.start()