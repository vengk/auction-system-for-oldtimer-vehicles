import socket
import threading
import time
from datetime import datetime
import struct
import json

def connectServer(ServerAddress, ServerPort):
    
    #Socket aufbauen
    ClientSocket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    # Message senden
    message = "Hallo Server ich moechte teilnehmen."
    ClientSocket.sendto(str.encode(message), (ServerAddress, ServerPort))
    #Auf Rückmeldung warten
    message, server = ClientSocket.recvfrom(1024)
    print(message.decode())
    
    ClientSocket_Bid = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    message, server = ClientSocket.recvfrom(1024)
    print(message.decode())
    
    while True:
        
        userInput = input("Bitte Fahrzeug auswählen: ")
        
        while True:
            bid = input("Bitte ein Gebot in Euro eingeben: ")
            print (bid.isnumeric())
            if bid.isnumeric():
                ClientSocket_Bid.sendto(str.encode(userInput + ";" + str(bid)), (ServerAddress, 1235))
                break
        print ("Kommt bis hier hin")
        while True:
            message, server = ClientSocket_Bid.recvfrom(1024)
            print(message.decode())
            break
            

        

if __name__ == '__main__':

    # Bind the socket to the port
    server_address = socket.gethostbyname(socket.gethostname())
    server_port = 1234

thread1 = threading.Thread(target = connectServer, args=(server_address, server_port))
thread1.start()
















class Client():
    def __init__(self):
        self.address = socket.gethostbyname(socket.gethostname()) 
        self.port = 7000
        self.message = "Hallo Server"

    def ConnectToBroadcast(self):
        broadcast_address = '255.255.255.255' 
        #Erstelle einen UDP-Socket
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) 
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1) 
        
        # Sende Daten an die Broadcast-Adresse und den Port    
        sock.sendto(self.address.encode(), (broadcast_address, self.port))
        sock.close()

    def Listen(self):
        # Broadcast-Adresse und Port
        broadcast_port = 5000
        
        # Erstelle einen UDP-Socket
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.bind((self.address, broadcast_port))
        
        # Setze den Socket, um Broadcast zu empfangen
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        
        while True:
            # Empfange Daten
            data, address = sock.recvfrom(1024)
            print(f"Empfangene Nachricht: {data.decode()} von {address}")

client = Client()            

thread1 = threading.Thread(target = client.ConnectToBroadcast)
#thread1.start()

thread2 = threading.Thread(target = client.Listen)
#thread2.start()