import socket
import threading
import time
from datetime import datetime
import struct
import json
# Server Klasse erstellt zum Verstehen: Server aufbauen
MULTICAST_GROUP_IP = '224.1.1.1'

clientList = []
auctionList = []

class Auction():
    def __init__(self, vehicle):
        self.auto = vehicle
        self.biddingList = []
        self.address = ""
    # Verwalten von Bids der Client
    def acceptBid(self, bid, address):
        if(self.biddingList == []):
            self.biddingList.append(bid)
            self.address = address
            return
        if(self.biddingList[-1] < bid):
            self.address = address
            self.biddingList.append(bid)
            return         

vehicle1 = Auction("Mercedes")
vehicle2 = Auction("BMW")

auctionList = [vehicle1, vehicle2]




class Server():
    def __init__(self):
        self.address = socket.gethostbyname(socket.gethostname()) # Lokale IP Adresse durch Funktionsaufruf gezogen
        self.message = "Hello World!"
        self.serverMessage = "Wants to connect"
        self.port = 10001
        #self.serverPort = 5000
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        #self.serverList = []
        #self.clientOffers = []
        #self.vehicles = []
        #self.bidList = []

    def acceptClient(self):
        ServerSocket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        ServerSocket.bind((socket.gethostname(), 1234))
        print("Server läuft!")

        while True:
            ClientSocket, address = ServerSocket.recvfrom(1024)
            ClientSocket = str(ClientSocket)
            print (ClientSocket)
            print (type(ClientSocket))
            if ClientSocket == "b'Hallo Server ich moechte teilnehmen.'":

                print(f"Verbindung zu {address} wurde hergestellt!")
                print (ClientSocket)
                clientList.append(address)
                print(clientList)
                ServerSocket.sendto(str.encode("Willkommen zur Auktion!"), address)    
            auctionResponse = "\n\n"
            for i in auctionList:
                print (i.auto)
                auctionResponse += i.auto
                if len(i.biddingList) > 0:
                    auctionResponse += " Aktuelles Gebot: " + i.biddingList[-1] + "\n"
                else:
                    auctionResponse += "\n"
            ServerSocket.sendto(str.encode(auctionResponse), address)

    def handleBid(self):
        ServerSocket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        ServerSocket.bind((socket.gethostname(), 1235))
        print("Auf Gebot warten...")

        while True:
            ClientSocket, address_bid = ServerSocket.recvfrom(1024)
            ClientSocket = str(ClientSocket)
            print ("Ausgabe: HandleBid-Funktion   " + ClientSocket)
            bid = ClientSocket.replace("'","").split(";")[-1]
            vehicle = ClientSocket.replace("'","").split(";")[0][1:]
            print (vehicle)
            auctionResponse = "\n\n"
            for i in auctionList:
                if i.auto == vehicle:
                    i.acceptBid(bid, address_bid)
                    print ("Gebot wurde abgegeben")
                auctionResponse += i.auto
                if len(i.biddingList) > 0:
                    auctionResponse += " Aktuelles Gebot: " + i.biddingList[-1] + "\n"
                else:
                    auctionResponse += "\n"
            print ("Gebote werden zurückgeschickt")
            ServerSocket.sendto(str.encode(auctionResponse), address_bid)







    def Listen(self):
        # Broadcast-Adresse und Port
        broadcast_address = '255.255.255.255'
        broadcast_port = 7000
        
        # Erstelle einen UDP-Socket
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.bind(('', broadcast_port))
        
        # Setze den Socket, um Broadcast zu empfangen
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)

        while True:
            # Empfange Daten
            data, address = sock.recvfrom(1024)
            if data:
                print(f"Empfangene Nachricht: {data.decode()} von {address}")
                reply_address = data.decode()
                replySocket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                replySocket.bind((reply_address,5000))
                replySocket.sendto(self.address.encode(), (reply_address,5000))
                replySocket.close()
                data = False



          
          











    def MulticastListenAndReply(self):
        # if my IP is not in the server list add it
        if self.address not in self.serverList:
            self.serverList.append(self.address)

        # create socket bind to server address
        multilis_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        multilis_sock.bind(('', self.serverPort))

        # tell the os to add the socket to the multicast group
        group = socket.inet_aton(MULTICAST_GROUP_IP)
        mreg = struct.pack('4sL', group, socket.INADDR_ANY)
        multilis_sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreg)
        
        while True:
                # receive data from other participants
                data, address = multilis_sock.recvfrom(1024)

                if data:
                    # if you have data than decode
                    print(data.decode())

    def MulticastSendAndReceive(self):
        # create socket
        multicast_group = (MULTICAST_GROUP_IP, self.serverPort)
        multisend_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) 
        multisend_sock.settimeout(2)

        # set time to live message (network hps; 1 for local)
        ttl = struct.pack('b', 1)
        multisend_sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, ttl)
        

        # send my IP to other participants
        message = "Hello World"
        multisend_sock.sendto(message.encode(), multicast_group)
        

    def ListenForServer(self):# Methode für neue Server 
        self.socket.bind((self.address, self.port))
        print('Server listening for other servers... {}:{}'.format(self.address, self.port))

        while True:
            print('\nWaiting to receive new server...\n')

            data, address = self.socket.recvfrom(1024)
            print('Received message from server: ', self.address)
            print('Server Message: ', data.decode())

            if data:
                self.socket.sendto(str.encode(self.message), self.address)
                print('Replied to client: ', self.message)

server = Server()

if __name__== '__main__':
    server = Server()

    #thread1 = threading.Thread(target = server.MulticastListenAndReply)
    #thread1.start()

    #thread2 = threading.Thread(target = server.MulticastSendAndReceive)
    #thread2.start()

    #thread3 = threading.Thread(target = server.Listen)
    #thread3.start()

    thread4 = threading.Thread(target = server.acceptClient)
    thread4.start()

    thread5 = threading.Thread(target = server.handleBid)
    thread5.start()